### LIGHT SHELL BACKGROUNDS

curated images that goes well with light theme, used along releases 
(light shell follow the current gnome release names).

### light shell 44 (alpha)

<img src=/assets/44.png>

[link1](https://unsplash.com/photos/zft-W1kVEhg) [link2](https://unsplash.com/photos/koy6FlCCy5s) [link3](https://unsplash.com/photos/DuBNA1QMpPA) [link4](https://unsplash.com/photos/uYl9tJsnBXk) [link5](https://unsplash.com/photos/wWd-socaX9U)

### light shell 43 (early development)

<img src=/assets/43.png>

[link1](https://gitlab.gnome.org/GNOME/gnome-backgrounds/-/blob/gnome-41/backgrounds/adwaita-morning.png) [link2](https://unsplash.com/photos/uRA-1WbLfYM) [link3](https://unsplash.com/photos/Q_LffdLyQqk) [link4](https://unsplash.com/photos/qzaIDFtzcZ0) [link5](https://unsplash.com/photos/v1CGTJaEFDo)

### light shell 42 (port of Elliot Shugerman "gnome shell default light")

<img src=/assets/42.png>

[link1](https://gitlab.gnome.org/GNOME/gnome-backgrounds/-/blob/gnome-42/backgrounds/adwaita-l.jpg) [link2](https://gitlab.gnome.org/GNOME/gnome-backgrounds/-/blob/gnome-42/backgrounds/adwaita-d.jpg) [link3](https://gitlab.gnome.org/GNOME/gnome-backgrounds/-/blob/gnome-3-32/backgrounds/adwaita-day.jpg?ref_type=heads) [link4](https://gitlab.gnome.org/GNOME/gnome-backgrounds/-/blob/gnome-3-28/backgrounds/adwaita-day.jpg?ref_type=heads) [link5](https://gitlab.gnome.org/GNOME/gnome-backgrounds/-/blob/gnome-3-20/backgrounds/Signpost_of_the_Shadows.jpg)

### Recommendation

- https://unsplash.com/
- https://gitlab.gnome.org/GNOME/gnome-backgrounds
- https://fedoraproject.org/wiki/Wallpapers

